extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	#Inicia Música
	$Musica.play(0)
	#Desativa Loop de Audios
	$SomJogo.stream.loop = false;
	$SomMenu.stream.loop = false;
	#Foca em um dos botoes
	$VBoxContainer/btnIniciar.grab_focus()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#Inicia o jogo
func _on_btnIniciar_pressed():
	$Musica.stop();
	get_tree().change_scene("res://Cenas/Mapa.tscn")


#Alterna Estado da Música durante o Jogo
func _on_btnMusica_pressed():
	if Global.musicEnabled:
		$VBoxContainer/btnMusica.text = Textos.MENU["music"]["off"]
		Global.musicEnabled = false;
		$Musica.stop()
	else:
		$VBoxContainer/btnMusica.text = Textos.MENU["music"]["on"]
		Global.musicEnabled = true;
		$Musica.play(0);
		pass
	pass # Replace with function body.

#Alterna o Estado dos Efeitos Sonoros Durante o jogo
func _on_btnSom_pressed():
	if Global.soundEnabled:
		$VBoxContainer/btnSom.text = Textos.MENU["sound"]["off"]
		Global.soundEnabled = false;
		$SomJogo.stop();
	else:
		$VBoxContainer/btnSom.text = Textos.MENU["sound"]["on"]
		Global.soundEnabled = true;
		$SomJogo.play(0)
	pass # Replace with function body.


#Toca som ao mudar de opção com o teclado caso esteja ativado o som
func _on_btn_focus_exited():
	if(Global.soundEnabled):
		$SomMenu.stop();
		$SomMenu.play(0);
	pass # Replace with function body.
