extends Node


const maxHelicoptero = 5; #Maximo de alunos no Helicoptero
var baseReturnAlertEmited = false; #Alerta de De Retorno já emitido

var timerLabel = null; #Label Do Relogio
var infoLabel = null; #Label De Info Extra

var onBase = false; #Está no IF

var musicEnabled = true; #Musica Habilitada?
var soundEnabled = true; #Som Habilitado?

var helicopterLanded = false; #Helicoptero Pousou?

var initialrun = true; 

var counters = {
	"spawn" : 0,
	"fall" : 0,
	"delivered" : 0,
	"onBoard" : 0
}

var counterObj = {
	"spawn" : null,
	"fall" : null,
	"delivered" : null,
	"onBoard" : null
}
# Called when the node enters the scene tree for the first time.
func _ready():
	preload("res://Cenas/InterfaceJogo.tscn")
	preload("res://Cenas/Mapa.tscn")
	randomize()
	pass # Replace with function body.

func setValueToLabel(key):
	counterObj[key].text = ("%s" % counters[key]);
	
func setTextToLabel(key,text):
	counterObj[key].text = text;
	
#Chamada para atualziar o relogio
func setTextToTimerLabel(time):
	var secs = "%02d" % fmod(time,60);
	var mins = "%02d" % floor(fmod(time,3600)/60);
	
	if(floor(fmod(time,3600)/60) < 1):
		timerLabel.add_color_override("font_color",Color("#99ffff"))
		pass
	timerLabel.text = "{min}:{sec}".format({"min": mins, "sec": secs})
	
#Insere Texto Qualquer no indicador
func setTextToInfoLabel(inf,color="ffffff"):
	infoLabel.add_color_override("font_color",Color(color))
	infoLabel.text = inf

#Insere Texto da Singleton Textos no indicador
func setTextConstantToInfoLabel(data):
	infoLabel.add_color_override("font_color",Color(data["color"]))
	infoLabel.text = data["text"]
