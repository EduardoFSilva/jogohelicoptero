extends Node

const HELICOPTERO = {
	"helicopter-full": {
		"text" : "Lotação esgotada. Retornar ao IFMG!",
		"color": "ffff99"
	},
	"student-deploy": {
		"text":"Deixando alunos no IF. Aguarde...",
		"color": "#99ff99"
	},
	"deploy-finished": {
		"text":"Liberado para partir",
		"color":"#99ff99"
	},
	"no-students": {
		"text":"Sem Alunos. Liberado para partir",
		"color":"#99ff99"
	},
	"out-of-bounds": {
		"text":"Fora da área do IFMG",
		"color":"ff9999"
	},
	"deploy-interaction":{
		"text":"Pressione 'Espaço' para deixar alunos no IF",
		"color":"#9999ff"
	}
}

const ALUNO = {
	"picked": {
		"text" : "Aluno Coletado!",
		"color": "9999ff"
	},
	"fall": {
		"text": "Aluno Caiu!",
		"color": "ffff99"
	}
}

const MENU = {
	"music": {
		"on": "Musica: ON",
		"off": "Musica: OFF"
	},
	"sound": {
		"on": "Som: ON",
		"off": "Som: OFF"
	}
}

func _ready():
	pass;
