extends Node2D

func _ready():
	pass

func _process(delta):
	if Global.onBase == true:
		self.rotation = deg2rad(0);
	else:
		#Posição Da Cruz Node Do IFMG
		var posIFMG = get_owner().get_owner().get_node("AreaIFMG").position;
		#Posição Da Cruz do Node do Helicoptero
		var posHelicoptero = get_owner().get_owner().get_node("Helicoptero").position;
		#Angulo em Radianos entre os dois pontos.
		var arctan = atan2(posIFMG.x-posHelicoptero.x,posHelicoptero.y-posIFMG.y);
		self.rotation = arctan;

#Toca o som do indicador de base
func playIndicatorSound(entering):
	$AudioIndicator.stop() #Para caso já esteja tocando
	if entering: #Se estiver Entrando na área
		$AudioIndicator.stream = load("res://Sons/SFX/Indicator-Enter.mp3")
	else: #Se estiver Saindo
		$AudioIndicator.stream = load("res://Sons/SFX/Indicator-Exit.mp3")
	$AudioIndicator.play() #Toca o audio
	#Aguarda parar de tocar
	yield(get_tree().create_timer($AudioIndicator.stream.get_length()), "timeout")
	#Para o audio
	$AudioIndicator.stop();

func _on_Area2D_body_entered():
	#Altera imagem para um alvo
	$Icone.texture = load("res://Imagens/waypoint.png")
	if Global.soundEnabled: #Verifica se o som está ligado
		playIndicatorSound(true) #Toca som de aproximação

func _on_Area2D_body_exited():
	#Altera o icone para a Seta
	$Icone.texture = load("res://Imagens/seta.png")
	if Global.soundEnabled: #Verifica se o som está ligado
		playIndicatorSound(false) #Toca som de aproximação

func _on_AudioIndicator_finished():
	pass
