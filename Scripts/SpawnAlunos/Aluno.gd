extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var canColide;

# Called when the node enters the scene tree for the first time.
func _ready():
	$SomAura.stream.loop = false;
	$SomExplosion.stream.loop = false;
	canColide = true;
	pass # Replace with function body.


#Colisao Comum De Todos os Alunos
func _on_common_body_entered(body):
	if(canColide): #Desativa a Colisão para que não seja possível haver multiplas
		canColide = false;
		Global.counters["spawn"] -= 1;
		#Configura o tamanho certo para a aura e explosion 
		#Verifica Os Limites Do Helicoptero
		#Se houve espaço, toca a animação AURA
		
		if(Global.counters["onBoard"] < Global.maxHelicoptero):
			playAnimationAndSound("onBoard",$SomAura,"aura","picked")
		#Se não houver espaço, toca a animação EXPLOSION
		else:
			playAnimationAndSound("fall",$SomExplosion,"explosion","fall")
	pass # Replace with function body.

func playAnimationAndSound(counter,sound,animation,text):
	Global.counters[counter] += 1;
	$Icon.play("empty");
	$Icon.stop();
	$Animation.play(animation)
	if(Global.soundEnabled):
		sound.play(0);
	Global.setTextConstantToInfoLabel(Textos.ALUNO[text])
	yield(get_tree().create_timer(0.5), "timeout")
	Global.setTextToInfoLabel("")
	if(Global.onBase):
		Global.setTextConstantToInfoLabel(Textos.HELICOPTERO["deploy-interaction"]);
	else:
		Global.setTextToInfoLabel("")
	pass

#Quando a animação de auta ou explosion acabar, destroi o objeto
func _on_animation_finished():
	if $Animation.animation == "aura" || $Animation.animation == "explosion":
		queue_free()
	pass # Replace with function body.
