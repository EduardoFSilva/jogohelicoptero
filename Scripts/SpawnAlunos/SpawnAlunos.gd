extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const maxAlunos = 200;
const alunosIniciais = 20;
const margin = 100
const timeoutValue = 5;

# Called when the node enters the scene tree for the first time.

func _process(delta):
	if(Global.initialrun):
		for i in range(0,alunosIniciais,1):
			gerarAlunos()
			Global.counters["spawn"] += 1
		Global.initialrun = false
	pass
	
	
func _ready():
	$Timer.wait_time = timeoutValue;
	$Timer.start()
	pass # Replace with function body.

func gerarAlunos():
	var aluno = get_node("Fabrica").gerarAlunoRand()
	aluno.position = Vector2(rand_range(0+margin,2583-margin),rand_range(0+margin,2277-margin))
	get_owner().get_node("AreaParaSpawn").add_child(aluno)
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Timer_timeout():
	if(maxAlunos > Global.counters["spawn"]):
		gerarAlunos();
		Global.counters["spawn"] += 1;
	pass # Replace with function body.
	
func _on_common_body_entered(body):
	pass # Replace with function body.


func _on_animation_finished():
	pass # Replace with function body.
