extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

#Gera um aluno especifico
func gerarAluno(valor):
	return get_children()[valor].duplicate();
	
#Seleciona Alunos Aleatórios
func gerarAlunoRand():
	return get_children()[randi() % get_children().size()].duplicate()
