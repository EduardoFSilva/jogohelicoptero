extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	#INICIALIZA LABELS NA CLASSE GLOBAL APÓS O PRELOAD
	Global.counterObj["spawn"] = get_node("ContainerMarcadores/LabelsCount/lbSpawn");
	Global.counterObj["fall"] = get_node("ContainerMarcadores/LabelsCount/lbFall");
	Global.counterObj["delivered"] = get_node("ContainerMarcadores/LabelsCount/lbDelivered");
	Global.counterObj["onBoard"] = get_node("ContainerMarcadores/LabelsCount/lbOnBoard");
	Global.timerLabel = get_node("TimerLabel")
	Global.infoLabel = get_node("lbInfo");
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#Eventos de colisao - saida
func _on_AreaIFMG_body_exited(body):
	if body.name == "ObjetoHelicoptero":
		Global.onBase = false #Helicoptero fora da base
		Global.setTextToInfoLabel("")
		#Chama O mesmo método em flecha direcao
		get_node("ContainerFlecha/FlechaDirecao")._on_Area2D_body_exited();

#Eventos de colisao - entrada
func _on_AreaIFMG_body_entered(body):
	if body.name == "ObjetoHelicoptero":
		Global.onBase = true #Helicoptero na Base
		Global.setTextConstantToInfoLabel(Textos.HELICOPTERO["deploy-interaction"])
		#Chama O mesmo método em flecha direcao
		get_node("ContainerFlecha/FlechaDirecao")._on_Area2D_body_entered();

