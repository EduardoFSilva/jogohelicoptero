extends Node2D

func _ready():
	#Se Habilitada, Inicia a Música do Mapa
	if(Global.musicEnabled):
		$MusicPlayer.play()
	
func _process(delta):
	#Muda valores na Label a todo frame
	Global.setTextToTimerLabel($Timer.time_left)
	Global.setValueToLabel("spawn")
	Global.setValueToLabel("fall")
	Global.setValueToLabel("delivered")
	Global.setTextToLabel("onBoard",("{value}/{max}".format({"value":Global.counters["onBoard"],"max":Global.maxHelicoptero})))
