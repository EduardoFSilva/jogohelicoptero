extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if body.name == "ObjetoHelicoptero":
		#Aumenta Zoom Ao Entrar na Área da Base
		get_node("../Helicoptero/Camera2D").get_node("AnimationPlayer").play("AniZoomBaseIFMG");
		pass



func _on_Area2D_body_exited(body):
	if body.name == "ObjetoHelicoptero":
		#Diminui Zoom Ao Sair da Área da Base
		get_node("../Helicoptero/Camera2D").get_node("AnimationPlayer").play_backwards("AniZoomBaseIFMG");
		pass

