extends AudioStreamPlayer2D

#Flag indicando se o audio já está afinado
var pitched = false;

func _ready():
	#Se o som estiver ligado inicia o som da asa rotativa
	if Global.soundEnabled: 
		self.play();
		self.pitch_scale = 0.67;
	pass
	
func _process(delta):
		#Executa somente se o som estiver ligado
		if Global.soundEnabled: 
			#Caso alguma tecla seja pressionada
			if Input.is_action_pressed("ui_up") || Input.is_action_pressed("ui_down") || Input.is_action_pressed("ui_left") || Input.is_action_pressed("ui_right"):
				#E o helicoptero não esteja pousado
				if !pitched && !Global.helicopterLanded:
					self.pitch_scale = 1.0; #Aumenta a velocidade do som
					pitched = true
				#Se está afinado e pousado
				elif pitched && Global.helicopterLanded:
					self.pitch_scale = 0.67; #Diminui a velocidade do som
					pitched = false
			#Caso nenhuma tecla esteja apertada, diminui a velocidade do som
			else:
				self.pitch_scale = 0.67;
				pitched = false
