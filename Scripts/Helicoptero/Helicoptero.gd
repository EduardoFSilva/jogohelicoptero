extends Node2D

#Enumera Direções
enum dirs{NORTH,SOUTH,EAST,WEST,NORTHEAST,NORTHWEST,SOUTHEAST,SOUTHWEST}
#Enumera Valores padrão de Grau para cada direcao
const directionValues = {dirs.NORTH: 0, dirs.SOUTH: 180, dirs.WEST: 270, dirs.EAST: 90,dirs.NORTHEAST:45,dirs.NORTHWEST:315,dirs.SOUTHEAST:135,dirs.SOUTHWEST:225};

var prevDirection = dirs.NORTH #Direcao Antiga Do Helicoptero
var currentDir = dirs.NORTH #Nova Direcao

const helicopterBondryDelta = 40; #Margem extra para evitar que o helicoptero saia do mapa

#Quantos Graus O Helicopter irá girar por frame
const rotationDelta = 5;
#Tempo que leva para o helicoptero girar no proprio eixo
const rotationFrameDelay = 20;
#Valor de Velocidade Perpenticular Em que o Helicoptero anda
const cardinalmoveSpeed = 3.75;
#Valor aproximado de cardinalMoveSpeed * sqrt(2) / 2
#para manter mesma velocidade diagonal
const colateralmoveSpeed = stepify((cardinalmoveSpeed * sin(45)),0.001);

func _ready():
	$DeploySound.stream.loop = false; #Desativa o Loop do som de descarga
	$TripleBeepSound.stream.loop = false; #Desativa Loop do bip triplo
	#scale = Vector2(onAirSize,onAirSize)
	pass # Replace with function body.

#Processamento de Teclas
func _process(delta):
	#PROCESSAMENTO DIRECIONAL
	#NORTH
	if (Input.is_action_pressed("ui_up") && !Input.is_action_pressed("ui_down") && xnor(Input.is_action_pressed("ui_left"), Input.is_action_pressed("ui_right"))):
		processDirection(dirs.NORTH)
		processMoviment(dirs.NORTH,false)
	#SOUTH
	if  (!Input.is_action_pressed("ui_up") && Input.is_action_pressed("ui_down") && xnor(Input.is_action_pressed("ui_left"),Input.is_action_pressed("ui_right"))):
		processDirection(dirs.SOUTH)
		processMoviment(dirs.SOUTH,false)
	#WEST
	if  (xnor(Input.is_action_pressed("ui_up"),Input.is_action_pressed("ui_down")) && Input.is_action_pressed("ui_left") && !Input.is_action_pressed("ui_right")):
		processDirection(dirs.WEST)
		processMoviment(dirs.WEST,false)
	#EAST
	if  (xnor(Input.is_action_pressed("ui_up"),Input.is_action_pressed("ui_down")) && !Input.is_action_pressed("ui_left") && Input.is_action_pressed("ui_right")):
		processDirection(dirs.EAST)
		processMoviment(dirs.EAST,false)
	#NORTHWEST
	if Input.is_action_pressed("ui_up") && !Input.is_action_pressed("ui_down") && Input.is_action_pressed("ui_left") && !Input.is_action_pressed("ui_right"):
		processDirection(dirs.NORTHWEST)
		processMoviment(dirs.NORTHWEST,true)
	#SOUTHWEST
	if !Input.is_action_pressed("ui_up") && Input.is_action_pressed("ui_down") && Input.is_action_pressed("ui_left") && !Input.is_action_pressed("ui_right"):
		processDirection(dirs.SOUTHWEST)
		processMoviment(dirs.SOUTHWEST,true)
	#NORTHEAST
	if Input.is_action_pressed("ui_up") && !Input.is_action_pressed("ui_down") && !Input.is_action_pressed("ui_left") && Input.is_action_pressed("ui_right"):
		processDirection(dirs.NORTHEAST)
		processMoviment(dirs.NORTHEAST,true)
	#SOUTHEAST
	if !Input.is_action_pressed("ui_up") && Input.is_action_pressed("ui_down") && !Input.is_action_pressed("ui_left") && Input.is_action_pressed("ui_right"):
		processDirection(dirs.SOUTHEAST)
		processMoviment(dirs.SOUTHEAST,true)
	#Processamento de Comandos
	if Input.is_action_pressed("deploy"):
		if !Global.helicopterLanded:
			deployAlunos();
	emitFullAlert();

func emitFullAlert():
	if(Global.counters["onBoard"] == Global.maxHelicoptero):
		if !Global.baseReturnAlertEmited:
			Global.baseReturnAlertEmited = true;
			yield(get_tree().create_timer(0.5), "timeout")
			Global.setTextConstantToInfoLabel(Textos.HELICOPTERO["helicopter-full"])
			playTripleBeepSound();
			yield(get_tree().create_timer(2), "timeout")
			Global.setTextToInfoLabel("")
		

func deployAlunos():
	#Se o Helocoptero está na base e contem alunos
	if(Global.onBase && Global.counters["onBoard"] > 0):
		#Liga o modo de Pouso Emite a Mensagem
		Global.helicopterLanded = true;
		Global.setTextConstantToInfoLabel(Textos.HELICOPTERO["student-deploy"])
		#Faz o Helicoptero Abaixar
		$Animation.play("Aproximacao");
		yield(get_tree().create_timer(0.5), "timeout")
		#Descarrega os alunos a bordo 1 a 1
		var alunoOnBoard = Global.counters["onBoard"];
		for i in range(0,alunoOnBoard,1):
			#Faz enquanto a quantidade a bordo for maior que 0
			if(Global.counters["onBoard"] > 0):
				playDeploySound();
				Global.counters["onBoard"] -= 1;
				Global.counters["delivered"] += 1
				yield(get_tree().create_timer(0.125), "timeout");
		#Faz o Helicoptero Subir
		$Animation.play_backwards("Aproximacao");
		yield(get_tree().create_timer(0.5), "timeout")
		#Desativa o Modo de pouso e alerta de retorno a base
		Global.helicopterLanded = false;
		Global.baseReturnAlertEmited = false;
		#Mostra texto e emite so,
		Global.setTextConstantToInfoLabel(Textos.HELICOPTERO["deploy-finished"])
		playTripleBeepSound(1.3)
	#Emite Este Alerta Caso Esteja na base mas o helicoptero está vazio
	elif(Global.onBase && Global.counters["onBoard"] == 0):
		Global.setTextConstantToInfoLabel(Textos.HELICOPTERO["no-students"])
		playTripleBeepSound(1.3)
	#Caso esteja fora da Base
	else: #Se Estiver Fora Da Area Do IF
		playTripleBeepSound(1.5)
		Global.setTextConstantToInfoLabel(Textos.HELICOPTERO["out-of-bounds"])
		yield(get_tree().create_timer(2), "timeout")
		Global.setTextToInfoLabel("")		
	
# Gira o Helicoptero
func processDirection(direction):
	if !Global.helicopterLanded:
		prevDirection = currentDir;
		currentDir = direction;
		var ret = getRotationData(prevDirection,currentDir);
		if (prevDirection != currentDir):
			if(ret["mult"] < 0):
				for i in range(ret["oldDeg"],ret["newDeg"],rotationDelta):
					self.rotation = deg2rad(i + rotationDelta)
	# warning-ignore:integer_division
					yield(get_tree().create_timer(rotationFrameDelay/1000*ret["fortyfives"]), "timeout")
			elif(ret["mult"] > 0):
				for i in range(ret["oldDeg"],ret["newDeg"],-rotationDelta):
					self.rotation = deg2rad(i - rotationDelta)
	# warning-ignore:integer_division
					yield(get_tree().create_timer(rotationFrameDelay/1000*ret["fortyfives"]), "timeout")
		pass

#Move Helicoptero nas 8 Direções
func processMoviment(direction, isColateral):
	if !Global.helicopterLanded:
		var moveSpeed = cardinalmoveSpeed;
		var moveVector = Vector2(0,0);
		if(isColateral):
			moveSpeed = colateralmoveSpeed;
		if(direction == dirs.NORTH):
			moveVector = Vector2(0,-1)
		elif(direction == dirs.SOUTH):
			moveVector = Vector2(0,1)
		elif(direction == dirs.WEST):
			moveVector = Vector2(-1,0)
		elif(direction == dirs.EAST):
			moveVector = Vector2(1,0)
		if(direction == dirs.NORTHWEST):
			moveVector = Vector2(-1,-1)
		elif(direction == dirs.SOUTHWEST):
			moveVector = Vector2(-1,1);
		elif(direction == dirs.NORTHEAST):
			moveVector = Vector2(1,-1)
		elif(direction == dirs.SOUTHEAST):
			moveVector = Vector2(1,1)
		
		#Anula uma das direções caso tenha colidido com o bordo do mapa
		#Bordo Oeste
		if(self.position.x < (0 + helicopterBondryDelta) && moveVector.x == -1):
			moveVector.x = 0;
		#Bordo Leste
		if(self.position.x > (2583 - helicopterBondryDelta) && moveVector.x == 1):
			moveVector.x = 0;
		#Bordo Norte
		if(self.position.y < (0 + helicopterBondryDelta) && moveVector.y == -1):
			moveVector.y = 0;
		#Bordo Sul
		if(self.position.y > (2277 - helicopterBondryDelta) && moveVector.y == 1):
			moveVector.y = 0;
		
		#Executa um NAND para reverter a velocidade de volta a cardinal se necessário
		if(!(moveVector.x != 0 && moveVector.y != 0)):
			moveSpeed = cardinalmoveSpeed;
		
		#Movimenta o Helicoptero	
		translate(moveVector * moveSpeed);

#Calcula a nova posição do Helicoptero com base na Lookup Table
func getRotationData(oldDir, newDir):
	var ret = {"oldDeg":0,"newDeg":0,"mult":1,"fortyfives":0}
	var retLktb = getDirectionDataFromLookUpTable(oldDir,newDir);
	if(retLktb == null):
		ret["oldDeg"] = directionValues[oldDir]
		ret["newDeg"] = directionValues[newDir]
		return ret
	ret["oldDeg"] = retLktb["oldDeg"];
	ret["newDeg"] = retLktb["newDeg"];
	ret["mult"] = retLktb["mult"];
	ret["fortyfives"] = retLktb["fortyfives"];
	return ret

#XOR Lógico
func xor(b1, b2):
	return (b1 == true && b2 == false) || (b1 == false && b2 == true)
	
#XNOR Lógico
func xnor(b1, b2):
	return (b1 == false && b2 == false) || (b1 == true && b2 == true)

#Entrou na área do IF
func _on_Area2D_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if body.name == "ObjetoHelicoptero":
		pass

#Saiu da Área do IF
func _on_Area2D_body_shape_exited(body_rid, body, body_shape_index, local_shape_index):
	if body.name == "ObjetoHelicoptero":
		
		pass

#Toca som de bip triplo para toda a classe
func playTripleBeepSound(pitch=1.0):
	if(Global.soundEnabled):
		$TripleBeepSound.pitch_scale = pitch;
		$TripleBeepSound.play(0)

#Toca o som de Deploy para toda a classe
func playDeploySound():
	if(Global.soundEnabled):
		$DeploySound.play(0);

#LOOKUP TABLE DE DIREÇÕES PRÉ-CALCULADAS
func getDirectionDataFromLookUpTable(oldDir,newDir):
	var directions = {
			dirs.NORTH: {
				dirs.NORTH: null,
				dirs.SOUTH: {"oldDeg":0,"newDeg":180,"mult":-1,"fortyfives":4}, 
				dirs.EAST: {"oldDeg":0,"newDeg":90,"mult":-1,"fortyfives":2}, 
				dirs.WEST: {"oldDeg":360,"newDeg":270,"mult":1,"fortyfives":2}, 
				dirs.NORTHWEST: {"oldDeg":360,"newDeg":315,"mult":1,"fortyfives":1}, 
				dirs.NORTHEAST: {"oldDeg":0,"newDeg":45,"mult":-1,"fortyfives":1}, 
				dirs.SOUTHWEST: {"oldDeg":360,"newDeg":225,"mult":1,"fortyfives":3}, 
				dirs.SOUTHEAST: {"oldDeg":0,"newDeg":135,"mult":-1,"fortyfives":3} 
			},
			dirs.SOUTH: {
				dirs.NORTH: {"oldDeg":180,"newDeg":0,"mult":1,"fortyfives":4}, 
				dirs.SOUTH: null,
				dirs.EAST: {"oldDeg":180,"newDeg":90,"mult":1,"fortyfives":2}, 
				dirs.WEST: {"oldDeg":180,"newDeg":270,"mult":-1,"fortyfives":2}, 
				dirs.NORTHWEST: {"oldDeg":180,"newDeg":315,"mult":-1,"fortyfives":3}, 
				dirs.NORTHEAST: {"oldDeg":180,"newDeg":45,"mult":1,"fortyfives":3}, 
				dirs.SOUTHWEST: {"oldDeg":180,"newDeg":225,"mult":-1,"fortyfives":1}, 
				dirs.SOUTHEAST: {"oldDeg":180,"newDeg":135,"mult":1,"fortyfives":1} 
			},
			
			dirs.EAST: {
				dirs.NORTH: {"oldDeg":90,"newDeg":0,"mult":1,"fortyfives":2}, 
				dirs.SOUTH: {"oldDeg":90,"newDeg":180,"mult":-1,"fortyfives":2}, 
				dirs.EAST: null,
				dirs.WEST: {"oldDeg":90,"newDeg":270,"mult":-1,"fortyfives":4}, 
				dirs.NORTHWEST: {"oldDeg":90,"newDeg":-45,"mult":1,"fortyfives":3}, 
				dirs.NORTHEAST: {"oldDeg":90,"newDeg":45,"mult":1,"fortyfives":1}, 
				dirs.SOUTHWEST: {"oldDeg":90,"newDeg":225,"mult":-1,"fortyfives":3}, 
				dirs.SOUTHEAST: {"oldDeg":90,"newDeg":135,"mult":-1,"fortyfives":1} 
			},
			dirs.WEST: {
				dirs.NORTH: {"oldDeg":270,"newDeg":360,"mult":-1,"fortyfives":2}, 
				dirs.SOUTH: {"oldDeg":270,"newDeg":180,"mult":1,"fortyfives":2}, 
				dirs.EAST: {"oldDeg":270,"newDeg":90,"mult":1,"fortyfives":4}, 
				dirs.WEST: null,
				dirs.NORTHWEST: {"oldDeg":270,"newDeg":315,"mult":-1,"fortyfives":1}, 
				dirs.NORTHEAST: {"oldDeg":-90,"newDeg":45,"mult":-1,"fortyfives":3}, 
				dirs.SOUTHWEST: {"oldDeg":270,"newDeg":225,"mult":1,"fortyfives":1}, 
				dirs.SOUTHEAST: {"oldDeg":270,"newDeg":135,"mult":1,"fortyfives":3}  
			},
			dirs.NORTHWEST: {
				dirs.NORTH: {"oldDeg":315,"newDeg":360,"mult":-1,"fortyfives":1}, 
				dirs.SOUTH: {"oldDeg":315,"newDeg":180,"mult":1,"fortyfives":3}, 
				dirs.EAST: {"oldDeg":-45,"newDeg":90,"mult":-1,"fortyfives":3}, 
				dirs.WEST: {"oldDeg":315,"newDeg":270,"mult":1,"fortyfives":1}, 
				dirs.NORTHWEST: null,
				dirs.NORTHEAST: {"oldDeg":-45,"newDeg":45,"mult":-1,"fortyfives":2}, 
				dirs.SOUTHWEST: {"oldDeg":315,"newDeg":225,"mult":1,"fortyfives":2}, 
				dirs.SOUTHEAST: {"oldDeg":315,"newDeg":135,"mult":1,"fortyfives":4} 
			},
			dirs.NORTHEAST: {
				dirs.NORTH: {"oldDeg":45,"newDeg":0,"mult":1,"fortyfives":1}, 
				dirs.SOUTH: {"oldDeg":45,"newDeg":180,"mult":-1,"fortyfives":3}, 
				dirs.EAST: {"oldDeg":45,"newDeg":90,"mult":-1,"fortyfives":1}, 
				dirs.WEST: {"oldDeg":45,"newDeg":-90,"mult":1,"fortyfives":3}, 
				dirs.NORTHWEST: {"oldDeg":45,"newDeg":-90,"mult":1,"fortyfives":3}, 
				dirs.NORTHEAST: null,
				dirs.SOUTHWEST: {"oldDeg":45,"newDeg":225,"mult":-1,"fortyfives":4}, 
				dirs.SOUTHEAST: {"oldDeg":45,"newDeg":135,"mult":-1,"fortyfives":2} 
			},
			
			dirs.SOUTHWEST: {
				dirs.NORTH: {"oldDeg":225,"newDeg":360,"mult":-1,"fortyfives":3}, 
				dirs.SOUTH: {"oldDeg":225,"newDeg":180,"mult":1,"fortyfives":1}, 
				dirs.EAST: {"oldDeg":225,"newDeg":90,"mult":1,"fortyfives":3}, 
				dirs.WEST: {"oldDeg":225,"newDeg":270,"mult":-1,"fortyfives":1}, 
				dirs.NORTHWEST: {"oldDeg":225,"newDeg":315,"mult":-1,"fortyfives":2}, 
				dirs.NORTHEAST: {"oldDeg":225,"newDeg":45,"mult":1,"fortyfives":4}, 
				dirs.SOUTHWEST: null,
				dirs.SOUTHEAST: {"oldDeg":225,"newDeg":135,"mult":1,"fortyfives":2} 
			},
			dirs.SOUTHEAST: {
				dirs.NORTH: {"oldDeg":135,"newDeg":0,"mult":1,"fortyfives":3}, 
				dirs.SOUTH: {"oldDeg":135,"newDeg":180,"mult":-1,"fortyfives":1}, 
				dirs.EAST: {"oldDeg":135,"newDeg":90,"mult":1,"fortyfives":1}, 
				dirs.WEST: {"oldDeg":135,"newDeg":270,"mult":-1,"fortyfives":3}, 
				dirs.NORTHWEST: {"oldDeg":135,"newDeg":315,"mult":-1,"fortyfives":4}, 
				dirs.NORTHEAST: {"oldDeg":135,"newDeg":45,"mult":1,"fortyfives":2}, 
				dirs.SOUTHWEST: {"oldDeg":135,"newDeg":225,"mult":-1,"fortyfives":2}, 
				dirs.SOUTHEAST: null
			}
		}
	return directions[oldDir][newDir]
