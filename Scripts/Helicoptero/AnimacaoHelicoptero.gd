extends AnimatedSprite

#Flag indicando se as asas do helicoptero já estão aceleradas
var accelerated = false;

func _ready():
	self.speed_scale = 4; #Inicia forçando a velocidade padrão
	self.play(); #Inicia a Animação
	pass
	
func _process(delta):
		#Caso alguma tecla de navegação seja pressionada
		if Input.is_action_pressed("ui_up") || Input.is_action_pressed("ui_down") || Input.is_action_pressed("ui_left") || Input.is_action_pressed("ui_right"):
			#E o Helicoptero não está pousado ou já acelerando
			if !accelerated && !Global.helicopterLanded:
				self.speed_scale = 16 #Acelera as asas
				accelerated = true #Altera a flag
			#Se já está acelerado e o helicoptero pousou
			elif accelerated && Global.helicopterLanded:
				self.speed_scale = 4; #Retorna a velocidade padrão
				accelerated = false #Alterna a Flag
		#Se nenhuma tecla de Movimento está pressionada
		else:
			self.speed_scale = 4; #Retorna a velocidade padrão
			accelerated = false #Alterna a Flag
